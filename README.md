# gitlab-cleanup-handler

[![Build Status](https://gitlab.com/unboundsoftware/gitlab-cleanup-handler/badges/master/pipeline.svg)](https://gitlab.com/unboundsoftware/gitlab-cleanup-handler/commits/master)
[![codecov](https://codecov.io/gl/unboundsoftware/gitlab-cleanup-handler/branch/master/graph/badge.svg)](https://codecov.io/gl/unboundsoftware/gitlab-cleanup-handler)

A small container which checks which images from Gitlab container registry
are used in the provided namespaces and updates the cleanup policies for
those projects.
