package kube

import "strings"

type ImageCollector map[string]map[string]struct{}

func NewImageCollector() ImageCollector {
	return make(map[string]map[string]struct{})
}

func (c *ImageCollector) Add(image string) {
	parts := strings.Split(image[20:], ":")
	if x, exists := (*c)[parts[0]]; exists {
		x[parts[1]] = struct{}{}
	} else {
		(*c)[parts[0]] = map[string]struct{}{
			parts[1]: {},
		}
	}
}

func (c *ImageCollector) Images() map[string][]string {
	images := make(map[string][]string)
	for i, x := range *c {
		for v := range x {
			images[i] = append(images[i], v)
		}
	}
	return images
}
