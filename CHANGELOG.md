# Changelog

All notable changes to this project will be documented in this file.

## [0.0.9] - 2025-03-03

### 🐛 Bug Fixes

- *(deps)* Update module github.com/alecthomas/kong to v1.7.0
- *(deps)* Update Go version to 1.23.6 in go.mod
- *(deps)* Update module github.com/alecthomas/kong to v1.8.0
- *(deps)* Update kubernetes packages to v0.32.2
- *(deps)* Update module github.com/alecthomas/kong to v1.8.1
- *(docker)* Use amd64 golang base image for build

## [0.0.8] - 2025-01-17

### 🐛 Bug Fixes

- *(deps)* Update module github.com/alecthomas/kong to v1.6.1
- *(deps)* Update kubernetes packages to v0.32.1

## [0.0.7] - 2024-12-28

### 🐛 Bug Fixes

- *(deps)* Update module github.com/stretchr/testify to v1.10.0
- *(deps)* Update module github.com/alecthomas/kong to v1.5.0
- *(deps)* Update module github.com/alecthomas/kong to v1.5.1
- *(deps)* Update module github.com/alecthomas/kong to v1.6.0
- *(deps)* Update kubernetes packages to v0.31.4
- *(deps)* Update kubernetes packages to v0.32.0

### ⚙️ Miscellaneous Tasks

- Simplify CI configuration by removing unused variables
- *(golangci)* Increase timeout to 5 minutes

## [0.0.6] - 2024-11-21

### 🐛 Bug Fixes

- *(deps)* Update module github.com/alecthomas/kong to v1.3.0
- *(deps)* Update module github.com/alecthomas/kong to v1.4.0
- *(deps)* Update kubernetes packages to v0.31.3

### ⚙️ Miscellaneous Tasks

- Remove pre-commit stage and include Go pre-commit template

## [0.0.5] - 2024-10-24

### 🐛 Bug Fixes

- *(deps)* Update kubernetes packages to v0.31.2

## [0.0.4] - 2024-10-06

### 🐛 Bug Fixes

- Flaky test
- *(deps)* Update module github.com/stretchr/testify to v1.9.0
- *(deps)* Update module github.com/alecthomas/kong to v0.9.0
- *(deps)* Update kubernetes packages to v0.29.3
- *(deps)* Update module k8s.io/client-go to v0.29.4
- *(deps)* Update kubernetes packages to v0.30.0
- *(deps)* Update kubernetes packages to v0.30.1
- *(deps)* Update kubernetes packages to v0.30.2
- *(deps)* Update kubernetes packages to v0.30.3
- *(deps)* Update kubernetes packages to v0.31.0
- *(deps)* Update module github.com/alecthomas/kong to v1
- *(deps)* Update module github.com/alecthomas/kong to v1.2.0
- *(deps)* Update module github.com/alecthomas/kong to v1.2.1
- *(deps)* Update kubernetes packages to v0.31.1

### 💼 Other

- *(deps)* Bump github.com/stretchr/testify from 1.7.2 to 1.8.0
- *(deps)* Bump golang from 1.19.1 to 1.19.2
- *(deps)* Bump k8s.io/api from 0.25.2 to 0.25.3
- *(deps)* Bump k8s.io/client-go from 0.25.2 to 0.25.3
- *(deps)* Bump github.com/stretchr/testify from 1.8.0 to 1.8.1
- *(deps)* Bump github.com/alecthomas/kong from 0.6.1 to 0.7.0
- *(deps)* Bump golang from 1.19.2 to 1.19.3
- *(deps)* Bump k8s.io/client-go from 0.25.3 to 0.25.4
- *(deps)* Bump github.com/alecthomas/kong from 0.7.0 to 0.7.1
- *(deps)* Bump golang from 1.19.3 to 1.19.4
- *(deps)* Bump k8s.io/client-go from 0.25.4 to 0.26.0
- *(deps)* Bump golang from 1.19.4 to 1.19.5
- *(deps)* Bump k8s.io/client-go from 0.26.0 to 0.26.1
- *(deps)* Bump golang from 1.19.5 to 1.20.0
- *(deps)* Bump golang from 1.20.0 to 1.20.1
- *(deps)* Bump github.com/stretchr/testify from 1.8.1 to 1.8.2
- *(deps)* Bump k8s.io/client-go from 0.26.1 to 0.26.2
- *(deps)* Bump golang from 1.20.1 to 1.20.2
- *(deps)* Bump k8s.io/client-go from 0.26.2 to 0.26.3
- *(deps)* Bump golang from 1.20.2 to 1.20.3
- *(deps)* Bump k8s.io/client-go from 0.26.3 to 0.27.0
- *(deps)* Bump k8s.io/client-go from 0.27.0 to 0.27.1
- *(deps)* Bump golang from 1.20.3 to 1.20.4
- *(deps)* Bump k8s.io/client-go from 0.27.1 to 0.27.2
- *(deps)* Bump github.com/stretchr/testify from 1.8.2 to 1.8.3
- *(deps)* Bump github.com/stretchr/testify from 1.8.3 to 1.8.4
- *(deps)* Bump golang from 1.20.4 to 1.20.5
- *(deps)* Bump k8s.io/client-go from 0.27.2 to 0.27.3
- *(deps)* Bump github.com/alecthomas/kong from 0.7.1 to 0.8.0
- *(deps)* Bump golang from 1.20.5 to 1.20.6
- *(deps)* Bump k8s.io/client-go from 0.27.3 to 0.27.4
- *(deps)* Bump golang from 1.20.6 to 1.20.7
- *(deps)* Bump golang from 1.20.7 to 1.21.0
- *(deps)* Bump k8s.io/client-go from 0.27.4 to 0.28.0
- *(deps)* Bump k8s.io/client-go from 0.28.0 to 0.28.1
- *(deps)* Bump golang from 1.21.0 to 1.21.1
- *(deps)* Bump k8s.io/client-go from 0.28.1 to 0.28.2
- *(deps)* Bump golang from 1.21.1 to 1.21.2
- *(deps)* Bump github.com/alecthomas/kong from 0.8.0 to 0.8.1
- *(deps)* Bump golang from 1.21.2 to 1.21.3
- *(deps)* Bump k8s.io/client-go from 0.28.2 to 0.28.3
- *(deps)* Bump golang from 1.21.3 to 1.21.4
- *(deps)* Bump k8s.io/client-go from 0.28.3 to 0.28.4
- *(deps)* Bump golang from 1.21.4 to 1.21.5
- *(deps)* Bump k8s.io/client-go from 0.28.4 to 0.29.0
- *(deps)* Bump golang from 1.21.5 to 1.21.6
- *(deps)* Bump k8s.io/client-go from 0.29.0 to 0.29.1
- *(deps)* Bump golang from 1.21.6 to 1.22.0
- *(deps)* Bump k8s.io/client-go from 0.29.1 to 0.29.2

### ⚙️ Miscellaneous Tasks

- Remove some duplication and add a first few tests
- Add pre-commit config
- Add checking of pre-commit rules
- Add tests for handler
- Use Docker DinD version from variable
- Switch to manual rebases for Dependabot
- Update to Go 1.20.3 and golangci-lint 1.52.2
- Update pre-commit and fix golangci-lint
- Update golangci-lint
- Use pre-commit image
- Change repo for markdownlint and update golangci-lint
- Remove Dependabot config
- Add release flow

### 🛡️ Security

- *(deps)* [security] bump golang.org/x/net from 0.13.0 to 0.17.0

## [0.0.3] - 2022-09-27

### 🚀 Features

- Add all existing tags to exclusion filter

### 💼 Other

- *(deps)* Bump k8s.io/client-go from 0.25.0 to 0.25.1
- *(deps)* Bump k8s.io/client-go from 0.25.1 to 0.25.2

## [0.0.2] - 2022-09-09

### 🐛 Bug Fixes

- Add CA certificates

## [0.0.1] - 2022-09-09

### 🚀 Features

- Initial commit

<!-- generated by git-cliff -->
