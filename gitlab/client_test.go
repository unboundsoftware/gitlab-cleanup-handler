package gitlab

import (
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRestClient_UpdateCleanupPolicy(t *testing.T) {
	type args struct {
		project  string
		versions []string
	}
	tests := []struct {
		name    string
		args    args
		handler func(t *testing.T) http.HandlerFunc
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "success",
			args: args{
				project:  "unboundsoftware/dummy",
				versions: []string{"1.0", "1.1"},
			},
			handler: func(t *testing.T) http.HandlerFunc {
				return func(writer http.ResponseWriter, request *http.Request) {
					buff, err := io.ReadAll(request.Body)
					assert.NoError(t, err)
					assert.Equal(t, "{\"container_expiration_policy_attributes\":{\"cadence\":\"1d\",\"enabled\":true,\"keep_n\":10,\"older_than\":\"14d\",\"name_regex\":\".*\",\"name_regex_keep\":\"(main|master|1.0|1.1)\"}}\n", string(buff))
					writer.WriteHeader(http.StatusOK)
				}
			},
			wantErr: assert.NoError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			server := httptest.NewServer(tt.handler(t))
			defer server.Close()
			r := &RestClient{
				client:  http.DefaultClient,
				token:   "some-gitlab-token",
				baseUrl: server.URL,
			}
			tt.wantErr(t, r.UpdateCleanupPolicy(tt.args.project, tt.args.versions), fmt.Sprintf("UpdateCleanupPolicy(%v, %v)", tt.args.project, tt.args.versions))
		})
	}
}

func TestRestClient_GetTags(t *testing.T) {
	type args struct {
		project string
	}
	tests := []struct {
		name    string
		args    args
		handler func(t *testing.T) http.HandlerFunc
		want    []Tag
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "error",
			args: args{
				project: "unboundsoftware/dummy",
			},
			handler: func(t *testing.T) http.HandlerFunc {
				return func(writer http.ResponseWriter, request *http.Request) {
					writer.Header().Set("Content-Length", "23")
					writer.WriteHeader(http.StatusOK)
					_, _ = writer.Write([]byte("abc"))
				}
			},
			want: nil,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "invalid character 'a' looking for beginning of value")
			},
		},
		{
			name: "success",
			args: args{
				project: "unboundsoftware/dummy",
			},
			handler: func(t *testing.T) http.HandlerFunc {
				return func(writer http.ResponseWriter, request *http.Request) {
					writer.WriteHeader(http.StatusOK)
					_, _ = writer.Write([]byte(`[{"name":"1.0"},{"name": "1.1"}]`))
				}
			},
			want: []Tag{
				{Name: "1.0"},
				{Name: "1.1"},
			},
			wantErr: assert.NoError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			server := httptest.NewServer(tt.handler(t))
			defer server.Close()
			r := &RestClient{
				client:  http.DefaultClient,
				token:   "some-gitlab-token",
				baseUrl: server.URL,
			}
			got, err := r.GetTags(tt.args.project)
			if !tt.wantErr(t, err, fmt.Sprintf("GetTags(%v)", tt.args.project)) {
				return
			}
			assert.Equalf(t, tt.want, got, "GetTags(%v)", tt.args.project)
		})
	}
}
