package main

import (
	"context"
	"fmt"
	"testing"

	"github.com/apex/log"
	"github.com/stretchr/testify/assert"
	"gitlab.com/unboundsoftware/apex-mocks"

	"gitlab.com/unboundsoftware/gitlab-cleanup-handler/gitlab"
)

func Test_handle(t *testing.T) {
	type args struct {
		cli          CLI
		kubeClient   func(t *testing.T) KubeClient
		gitlabClient func(t *testing.T) GitlabClient
	}
	tests := []struct {
		name       string
		args       args
		wantLogged []string
		wantErr    assert.ErrorAssertionFunc
	}{
		{
			name: "unable to get images",
			args: args{
				cli: CLI{
					Namespaces: []string{"default"},
				},
				kubeClient: func(t *testing.T) KubeClient {
					return &MockKube{
						GetFn: func(ctx context.Context, logger log.Interface, namespaces []string) (map[string][]string, error) {
							assert.Equal(t, []string{"default"}, namespaces)
							return nil, fmt.Errorf("error")
						},
					}
				},
				gitlabClient: func(t *testing.T) GitlabClient {
					return nil
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "error")
			},
		},
		{
			name: "unable to fetch tags",
			args: args{
				cli: CLI{
					Namespaces: []string{"default"},
				},
				kubeClient: func(t *testing.T) KubeClient {
					return &MockKube{
						GetFn: func(ctx context.Context, logger log.Interface, namespaces []string) (map[string][]string, error) {
							return map[string][]string{
								"unboundsoftware/dummy": {"abc123"},
							}, nil
						},
					}
				},
				gitlabClient: func(t *testing.T) GitlabClient {
					return &MockGitlab{
						TagsFn: func(image string) ([]gitlab.Tag, error) {
							assert.Equal(t, "unboundsoftware/dummy", image)
							return nil, fmt.Errorf("tags error")
						},
					}
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "tags error")
			},
		},
		{
			name: "unable to update policy",
			args: args{
				cli: CLI{
					Namespaces: []string{"default"},
				},
				kubeClient: func(t *testing.T) KubeClient {
					return &MockKube{
						GetFn: func(ctx context.Context, logger log.Interface, namespaces []string) (map[string][]string, error) {
							return map[string][]string{
								"unboundsoftware/dummy": {"abc123"},
							}, nil
						},
					}
				},
				gitlabClient: func(t *testing.T) GitlabClient {
					return &MockGitlab{
						TagsFn: func(image string) ([]gitlab.Tag, error) {
							return []gitlab.Tag{
								{Name: "1.0"},
							}, nil
						},
						UpdateFn: func(project string, versions []string) error {
							assert.Equal(t, "unboundsoftware/dummy", project)
							assert.Equal(t, []string{"abc123", "1.0"}, versions)
							return fmt.Errorf("update error")
						},
					}
				},
			},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "update error")
			},
		},
		{
			name: "success",
			args: args{
				cli: CLI{
					Namespaces: []string{"default"},
				},
				kubeClient: func(t *testing.T) KubeClient {
					return &MockKube{
						GetFn: func(ctx context.Context, logger log.Interface, namespaces []string) (map[string][]string, error) {
							return map[string][]string{
								"unboundsoftware/dummy": {"abc123"},
							}, nil
						},
					}
				},
				gitlabClient: func(t *testing.T) GitlabClient {
					return &MockGitlab{
						TagsFn: func(image string) ([]gitlab.Tag, error) {
							return []gitlab.Tag{
								{Name: "1.0"},
							}, nil
						},
						UpdateFn: func(project string, versions []string) error {
							return nil
						},
					}
				},
			},
			wantErr: assert.NoError,
		},
	}
	for _, tt := range tests {
		logger := apex.New()
		t.Run(tt.name, func(t *testing.T) {
			kubeClient := tt.args.kubeClient(t)
			gitlabClient := tt.args.gitlabClient(t)
			tt.wantErr(t, handle(tt.args.cli, logger, kubeClient, gitlabClient), fmt.Sprintf("handle(%v, %v, %v, %v)", tt.args.cli, logger, kubeClient, gitlabClient))
		})
		logger.Check(t, tt.wantLogged)
	}
}

type MockKube struct {
	GetFn func(ctx context.Context, logger log.Interface, namespaces []string) (map[string][]string, error)
}

func (m *MockKube) GetImages(ctx context.Context, logger log.Interface, namespaces []string) (map[string][]string, error) {
	if m.GetFn == nil {
		return nil, nil
	}
	return m.GetFn(ctx, logger, namespaces)
}

var _ KubeClient = &MockKube{}

type MockGitlab struct {
	TagsFn   func(image string) ([]gitlab.Tag, error)
	UpdateFn func(project string, versions []string) error
}

func (m *MockGitlab) GetTags(image string) ([]gitlab.Tag, error) {
	if m.TagsFn == nil {
		return nil, nil
	}
	return m.TagsFn(image)
}

func (m *MockGitlab) UpdateCleanupPolicy(project string, versions []string) error {
	if m.UpdateFn == nil {
		return nil
	}
	return m.UpdateFn(project, versions)
}

var _ GitlabClient = &MockGitlab{}
